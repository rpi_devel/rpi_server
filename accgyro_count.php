<?php
$fileLine = "Waiting for data...";

// Copy the original file to a new file: PHP will use the new file to
// avoid to enter in conflict with the writer (C appliaction)
$origfile = './logs/application_max_acc.txt';
$newfile = './logs/application_max_acc_phpcopy.txt';

//	// Open the application report file and get size (OLD CONTENT)
//	$myfile = fopen("./logs/application_max_acc_phpcopy.txt", "r") or die("Unable to open file!");
//
//	$fileSize = filesize("./logs/application_max_acc_phpcopy.txt");
//
//	if ($fileSize > 0)
//	{
//		$fileLine = fgets($myfile, 1024);
//	}
//
//	// Output line
//	$fileLine = mb_convert_encoding($fileLine,"UTF-8","ISO-8859-1");
//
//	fclose($myfile);

	// Now copy the new file over and try to read
	if (copy($origfile, $newfile))
	{
		// Open the application report file and get size
		$myfile = fopen("./logs/application_max_acc_phpcopy.txt", "r") or die("Unable to open file!");

		$fileSize = filesize("./logs/application_max_acc_phpcopy.txt");

		if ($fileSize > 0)
		{
			$fileLine = fgets($myfile, 1024);
		}

		// Output line
		$fileLine = mb_convert_encoding($fileLine,"UTF-8","ISO-8859-1");

		fclose($myfile);
	}

echo $fileLine;
?>
