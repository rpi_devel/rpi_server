<?php
$usrParam = $_POST['myParam'];
$progParam1 = $_POST['accGyro_fs'];
$progParam2 = $_POST['accGyro_odr'];
$progParam3 = $_POST['accGyro_gyro_status'];

// Build the string to send to the FIFO: it is the command for the program on the otehr side of the FIFO
$cmdTosend = "CMD=" . $usrParam . "-" . "FS=" . $progParam1 . "-" . "ODR=" . $progParam2 .  "-" . "GYRO=" . $progParam3;

system("sudo sh -c 'echo \"" . escapeshellarg($cmdTosend) . "\" > /tmp/in_fifo'");

$data = array('first_param'=>$usrParam,
              'second_param'=>$progParam1,
              'third_param'=>$progParam2,
              'forth_param'=>$progParam3,
              'cmd_to_send'=>$cmdTosend,
             );

print json_encode($data);
?>
