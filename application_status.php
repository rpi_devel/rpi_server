<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

while (1)
{
	// Every second, sent a "ping" event.
	$time = time();

	// Check applicatino status
	if (processExists("rpi_sensors"))
	{
		$appStatus = "RUNNING";
	}
	else
	{
		$appStatus = "STOPPED";
	}

	echo "data: Application status is $appStatus @ time: {$time}\n\n";

	ob_flush();
	flush();
	sleep(2);
}

///
// Check if process exists
///
function processExists ($processName)
{
    $exists= false;

    exec("ps -A | grep -i $processName | grep -v grep", $pids);

    if (count($pids) > 0)
    {
        $exists = true;
    }
    return $exists;
}
?>
