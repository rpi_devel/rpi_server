<?php
// Header
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// Get params
$processToCheck = $_GET['get_param']; // $argv[1];

// Before to start the process check if name of the same process already exists, 
// in that case kill already existing pids (not gracefully)
do 
{
	if (processExists($processToCheck)) 
	{
		// Check if process exists
		$pid = getRunningPid($processToCheck);

		// Kill the process
        exec( "/home/pi/development/debugFromEclipse/rpi_sensors/quitter $pid" );
    	//exec ("kill -9 $pid");		
	}
	else
	{
		$pid = 0;
	}	
} while ($pid != 0);

// Now start the process
//shell_exec("/mnt/rpi-rootfs/home/pi/development/debugFromEclipse/rpi_sensors/launcher");
exec("/home/pi/development/debugFromEclipse/rpi_sensors/launcher" . " > /dev/null &");  

// Sleep for 0.5 seconds
usleep(500000);

// Get new pid
$newPid = getRunningPid($processToCheck);

// Return response           
$title = "Process data";
$killedProcessPid = "Killed pid: " . $pid;
$newProcessPid = "New pid: " . $newPid;

$data = array('title'=>$title,
              'killedProc'=>$killedProcessPid,
              'newProc'=>$newProcessPid,
             );

print json_encode($data);

///
// Check if process exists
///
function processExists ($processName) 
{
    $exists= false;

    exec("ps -A | grep -i $processName | grep -v grep", $pids);
    
    if (count($pids) > 0) 
    {
        $exists = true;
    }
    return $exists;
}

///
// Get PID for a running process
///
function getRunningPid ($processName) 
{
    $pid = 0;
    $processes = array();
    $command = 'ps ax | grep ' . $processName;
    exec($command, $processes);

    foreach ($processes as $processString) 
    {
        $processArr = explode(' ', trim($processString));

        if ((intval($processArr[0]) != getmypid()) &&
            (strpos($processString, 'grep '.$processName) === false)) 
        {
        	$pid = intval($processArr[0]);
    	}
    }

    return $pid;
}
?>