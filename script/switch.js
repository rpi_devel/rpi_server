///
// Manage the user press of the button to activate switches
///
var GPIO_CMD_OFF = 0;
var GPIO_CMD_ON = 1;

var numberOfExec = 0;

var auto_refresh_image;

var commandIsInExecution;

///
// Function to be called on timer
///
function GetSwitchData()
{
    // Signal start of request
    numberOfExec++;
    //console.log ("Start request #: " + numberOfExec);

    var userParam = "S";

    var channelNumber;
    var gpioNumber;
	var outputText;

    $.ajax({
        url: 'switch_status.php',
        type: "POST",
        dataType: "json",
        data: ({myParam: userParam}),
        success: function(data)
        {
            channelNumber = data["channelNumber"];
            gpioNumber = data["gpioNumber"];

            // Date and time
            //console.log ("Delta time in minutes: " + data["deltaTime"] + " - File size in lines: " + data["fileNumOfLines"] + " - Request status: " + avoidUpdateButtons);

            // Data is considered valid only if it is newer than a certain amount of minutes
            if (data["deltaTime"] < 3)
            {
                // Set display for channel 1
                //console.log( data["outputText"][0].status);
                console.log ("GetSwitchData() " + data["outputText"][0].status + " channel " + 1 + " - Executing command " + commandIsInExecution);

                if ("ON" == data["outputText"][0].status)
                {
                    document.getElementById('switchStatusCh1ImageImgId').src = "images/electrical_plug_lime.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh1ImageImgId').src = "images/switch_on.png";
                    }
                }
                else if ("OFF" == data["outputText"][0].status)
                {
                    document.getElementById('switchStatusCh1ImageImgId').src = "images/electrical_plug_black.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh1ImageImgId').src = "images/switch_off.png";
                    }
                }
                else
                {
                    document.getElementById('switchStatusCh1ImageImgId').src = "images/question_mark.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh1ImageImgId').src = "images/switch_off.png";
                    }
                }

                // Set display for channel 2
                //console.log( data["outputText"][1].status);

                if ("ON" == data["outputText"][1].status)
                {
                    document.getElementById('switchStatusCh2ImageImgId').src = "images/electrical_plug_lime.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh2ImageImgId').src = "images/switch_on.png";
                    }
                }
                else if ("OFF" == data["outputText"][1].status)
                {
                    document.getElementById('switchStatusCh2ImageImgId').src = "images/electrical_plug_black.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh2ImageImgId').src = "images/switch_off.png";
                    }
                }
                else
                {
                    document.getElementById('switchStatusCh2ImageImgId').src = "images/question_mark.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh2ImageImgId').src = "images/switch_off.png";
                    }
                }

                // Set display for channel 3
                //console.log( data["outputText"][2].status);

                if ("ON" == data["outputText"][2].status)
                {
                    document.getElementById('switchStatusCh3ImageImgId').src = "images/electrical_plug_lime.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh3ImageImgId').src = "images/switch_on.png";
                    }
                }
                else if ("OFF" == data["outputText"][2].status)
                {
                    document.getElementById('switchStatusCh3ImageImgId').src = "images/electrical_plug_black.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh3ImageImgId').src = "images/switch_off.png";
                    }
                }
                else
                {
                    document.getElementById('switchStatusCh3ImageImgId').src = "images/question_mark.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh3ImageImgId').src = "images/switch_off.png";
                    }
                }

                // Set display for channel 4
                //console.log( data["outputText"][3].status);

                if ("ON" == data["outputText"][3].status)
                {
                    document.getElementById('switchStatusCh4ImageImgId').src = "images/electrical_plug_lime.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh4ImageImgId').src = "images/switch_on.png";
                    }
                }
                else if ("OFF" == data["outputText"][3].status)
                {
                    document.getElementById('switchStatusCh4ImageImgId').src = "images/electrical_plug_black.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh4ImageImgId').src = "images/switch_off.png";
                    }
                }
                else
                {
                    document.getElementById('switchStatusCh4ImageImgId').src = "images/question_mark.png";
                    if (false == commandIsInExecution)
                    {
                        document.getElementById('switchControlCh4ImageImgId').src = "images/switch_off.png";
                    }
                }
            }
            else
            {
                document.getElementById('switchStatusCh1ImageImgId').src = "images/question_mark.png";
                document.getElementById('switchStatusCh2ImageImgId').src = "images/question_mark.png";
                document.getElementById('switchStatusCh3ImageImgId').src = "images/question_mark.png";
                document.getElementById('switchStatusCh4ImageImgId').src = "images/question_mark.png";
                document.getElementById('switchControlCh1ImageImgId').src = "images/switch_off.png";
                document.getElementById('switchControlCh2ImageImgId').src = "images/switch_off.png";
                document.getElementById('switchControlCh3ImageImgId').src = "images/switch_off.png";
                document.getElementById('switchControlCh4ImageImgId').src = "images/switch_off.png";
            }

            // We set command in executino to false, it was used to avoid false detection
            commandIsInExecution = false;
        },
        error: function(data)
        {
            console.log ("Error calling switch_status.php")
        }
    });

    // Signal end of request
    //console.log ("End request #: " + numberOfExec);

    // Start timer again
    setTimeout(GetSwitchData, 2000);
};

///
// jQuery: on document ready run a timer to execute the file read for the switch status
///
$(document).ready(function(){
    // Set globals
    commandIsInExecution = false;

    // Execute function once
    GetSwitchData ();

    // Start timer for that function in order to execute it every 2 seconds
    //auto_refresh_image = setInterval(
	//function ()
	//{
	//	GetSwitchData();
	//}, 2000);  // Refresh every 2000 milliseconds (1 sec is not enough to prevent false checking during command)
});

///
// Check the status of the switches calling 'switch_status.php' script.
// This script checks the content of a 'switch_status.txt' ascii file with content:
//    <#> <status: ON, OFF, DISABLED, UNKNOWN>
// Where # is the switch number and goes from 1 to 4 followed by status.
// This function performs 2 different aciotns:
//    - It change the image of the switch
//    - It calls the php script that write data to the FIFO for the xternal program
///
function changeImage(elemId)
{
    var image = document.getElementById(elemId);
    var channel = 0;

    // Stop interval
    //clearInterval(auto_refresh_image);

    // Set a variable to true to avoid execution of previous call
    commandIsInExecution = true;

    // Check channel
    if (elemId == 'switchControlCh1ImageImgId')
    {
        channel = 1;

        var plugImage = document.getElementById('switchStatusCh1ImageImgId');
    }
    else if (elemId == 'switchControlCh2ImageImgId')
    {
        channel = 2;

        var plugImage = document.getElementById('switchStatusCh2ImageImgId');
    }
    else if (elemId == 'switchControlCh3ImageImgId')
    {
        channel = 3;

        var plugImage = document.getElementById('switchStatusCh3ImageImgId');
    }
    else if (elemId == 'switchControlCh4ImageImgId')
    {
        channel = 4;

        var plugImage = document.getElementById('switchStatusCh4ImageImgId');
    }

    // If we have a valid channel
    if (channel > 0)
    {
        if (image.src.match("switch_on"))
        {
    		console.log ("changeImage() OFF channel " + channel);
            image.src = "images/switch_off.png";

            // We use the following values:
            // - myParam             : 'S'
            // - accGyro_fs          : channel number (1 to 4)
            // - accGyro_odr         : command (0 = OFF, 1 = ON)
            // - accGyro_gyro_status : RFU
            $.ajax({
                url: '../write_fifo.php',
                type: "POST",
                dataType: "json",
                data: ({myParam: "S", accGyro_fs: channel, accGyro_odr: GPIO_CMD_OFF, accGyro_gyro_status: 0}),
                success: function(data)
                {
                    //console.log (data["cmd_to_send"]);
                },
                error: function(data)
                {
                    console.log ("Error calling write_fifo.php")
                }
            });
        }
    	else
        {
    		console.log ("changeImage() ON channel " + channel);
            image.src = "images/switch_on.png";

            // We use the following values:
            // - myParam             : 'S'
            // - accGyro_fs          : channel number (1 to 4)
            // - accGyro_odr         : command (0 = OFF, 1 = ON)
            // - accGyro_gyro_status : RFU
            $.ajax({
                url: '../write_fifo.php',
                type: "POST",
                dataType: "json",
                data: ({myParam: "S", accGyro_fs: channel, accGyro_odr: GPIO_CMD_ON, accGyro_gyro_status: 0}),
                success: function(data)
                {
                    //console.log (data["cmd_to_send"]);
                },
                error: function(data)
                {
                    console.log ("Error calling write_fifo.php")
                }
            });
        }
    }

    // Start timer again
    //setTimeout(GetSwitchData, 3000);

    //auto_refresh_image = setInterval(
    //function ()
    //{
    //    GetSwitchData();
    //}, 2000); // Refresh every 2000 milliseconds (1 sec is not enough to prevent false checking during command)
}
