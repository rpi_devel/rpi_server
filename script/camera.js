// Variables for picture
var job = 0;
var autoIsSet = false;
var pictureNumber = 2;

// Variables for camera status management
var cameraStatus = localStorage.getItem('cameraStatusLSItem') || "ON";
var cameraAutoStatus = localStorage.getItem('cameraAutoStatusLSItem') || "AUTO";
var cameraIso = localStorage.getItem('cameraIsoLSItem') || "100";
var cameraFlashStatus = localStorage.getItem('cameraFlashStatusLSItem') || "AUTO";
var cameraPictureSize = localStorage.getItem('cameraPictureSizeLSItem') || "640x320";
var cameraExposure = localStorage.getItem('cameraExposureLSItem') || "AUTO";
var cameraWhiteBalance = localStorage.getItem('cameraWhiteBalanceLSItem') || "AUTO";
var cameraSharpness = localStorage.getItem('cameraSharpnessLSItem') || 0;
var cameraSaturation = localStorage.getItem('cameraSaturationLSItem') || 0;
var cameraBrightness = localStorage.getItem('cameraBrightnessLSItem') || 0;
var cameraContrast = localStorage.getItem('cameraContrastLSItem') || 0;
var cameraDrc = localStorage.getItem('cameraDrcLSItem') || "OFF";
var cameraShutterSpeed = localStorage.getItem('cameraShutterSpeedLSItem') || 4000;

///
// Local session storage: save and restore the page status
///
$(document).ready(function()
{
	///
	// Restore data
	///
	// Restore: camera status
	if (cameraStatus == "ON")
	{
		document.getElementById('cameraOnId').checked = true;
		document.getElementById('cameraOffId').checked = false;
	}
	else // OFF
	{
		document.getElementById('cameraOnId').checked = false;
		document.getElementById('cameraOffId').checked = true;
	}

	// Restore: camera auto status
	if (cameraAutoStatus == "AUTO")
	{
		document.getElementById('cameraAutoImgId').src = "/images/photo33.png";

		document.getElementById('cameraAutoAutoId').checked = true;
		document.getElementById('cameraAutoManualId').checked = false;
	}
	else // MANUAL
	{
		document.getElementById('cameraAutoImgId').src = "/images/users81.png";

		document.getElementById('cameraAutoAutoId').checked = false;
		document.getElementById('cameraAutoManualId').checked = true;
	}

	// Restore: cameraIso
	document.getElementById('cameraIso100Id').checked = false;
	document.getElementById('cameraIso200Id').checked = false;
	document.getElementById('cameraIso400Id').checked = false;
	document.getElementById('cameraIso800Id').checked = false;
	if (cameraIso == "100")
	{
		document.getElementById('cameraIso100Id').checked = true;
	}
	else if (cameraIso == "200")
	{
		document.getElementById('cameraIso200Id').checked = true;
	}
	else if (cameraIso == "400")
	{
		document.getElementById('cameraIso400Id').checked = true;
	}
	else if (cameraIso == "800")
	{
		document.getElementById('cameraIso800Id').checked = true;
	}

	// Restore: cameraFlashStatus
	document.getElementById('cameraFlashAutoId').checked = false;
	document.getElementById('cameraFlashOffId').checked = false;
	document.getElementById('cameraFlashOnId').checked = false;
	if (cameraFlashStatus == "AUTO")
	{
		document.getElementById('cameraFlashImgId').src = "/images/automatic5.png";
		document.getElementById('cameraFlashAutoId').checked = true;
	}
	else if (cameraFlashStatus == "OFF")
	{
		document.getElementById('cameraFlashImgId').src = "/images/lightning10.png";
		document.getElementById('cameraFlashOffId').checked = true;
	}
	else if (cameraFlashStatus == "ON")
	{
		document.getElementById('cameraFlashImgId').src = "/images/flash29.png";
		document.getElementById('cameraFlashOnId').checked = true;
	}

	// Restore: cameraPictureSize
	document.getElementById('cameraSizeAutoId').checked = false;
	document.getElementById('cameraSize2592x1944Id').checked = false;
	document.getElementById('cameraSize1920x1080Id').checked = false;
	document.getElementById('cameraSize1296x972Id').checked = false;
	document.getElementById('cameraSize1296x730Id').checked = false;
	document.getElementById('cameraSize640x480Id').checked = false;
	if (cameraPictureSize == "AUTO")
	{
		document.getElementById('cameraSizeAutoId').checked = true;
	}
	else if (cameraPictureSize == "2592x1944")
	{
		document.getElementById('cameraSize2592x1944Id').checked = true;
	}
	else if (cameraPictureSize == "1920x1080")
	{
		document.getElementById('cameraSize1920x1080Id').checked = true;
	}
	else if (cameraPictureSize == "1296x972")
	{
		document.getElementById('cameraSize1296x972Id').checked = true;
	}
	else if (cameraPictureSize == "1296x730")
	{
		document.getElementById('cameraSize1296x730Id').checked = true;
	}
	else if (cameraPictureSize == "640x480")
	{
		document.getElementById('cameraSize640x480Id').checked = true;
	}

	// Restore: cameraExposure
	document.getElementById('cameraExposureAutoId').checked = false;
	document.getElementById('cameraExposureNightId').checked = false;
	document.getElementById('cameraExposureVeryLongId').checked = false;
	document.getElementById('cameraExposureSpotLightId').checked = false;
	if (cameraExposure == "AUTO")
	{
		document.getElementById('cameraExposureImgId').src = "/images/camera69.png";
		document.getElementById('cameraExposureAutoId').checked = true;
	}
	else if (cameraExposure == "NIGHT")
	{
		document.getElementById('cameraExposureImgId').src = "/images/camera70.png";
		document.getElementById('cameraExposureNightId').checked = true;
	}
	else if (cameraExposure == "VERYLONG")
	{
		document.getElementById('cameraExposureImgId').src = "/images/photo108.png";
		document.getElementById('cameraExposureVeryLongId').checked = true;
	}
	else if (cameraExposure == "SPOTLIGHT")
	{
		document.getElementById('cameraExposureImgId').src = "/images/camera65.png";
		document.getElementById('cameraExposureSpotLightId').checked = true;
	}

	// Restore: cameraWhiteBalance
	document.getElementById('cameraWhiteBalanceAutoId').checked = false;
	document.getElementById('cameraWhiteBalanceOffId').checked = false;
	document.getElementById('cameraWhiteBalanceSunnyId').checked = false;
	document.getElementById('cameraWhiteBalanceFlashId').checked = false;
	document.getElementById('cameraWhiteBalanceShadeId').checked = false;
	document.getElementById('cameraWhiteBalanceCloudyId').checked = false;
	if (cameraWhiteBalance == "AUTO")
	{
		document.getElementById('cameraWhiteBalanceAutoId').checked = true;
	}
	else if (cameraWhiteBalance == "OFF")
	{
		document.getElementById('cameraWhiteBalanceOffId').checked = true;
	}
	else if (cameraWhiteBalance == "SUNNY")
	{
		document.getElementById('cameraWhiteBalanceSunnyId').checked = true;
	}
	else if (cameraWhiteBalance == "FLASH")
	{
		document.getElementById('cameraWhiteBalanceFlashId').checked = true;
	}
	else if (cameraWhiteBalance == "SHADE")
	{
		document.getElementById('cameraWhiteBalanceShadeId').checked = true;
	}
	else if (cameraWhiteBalance == "CLOUDY")
	{
		document.getElementById('cameraWhiteBalanceCloudyId').checked = true;
	}

	// Restore: cameraDrc
	document.getElementById('cameraDrcOffId').checked = false;
	document.getElementById('cameraDrcLowId').checked = false;
	document.getElementById('cameraDrcMediumId').checked = false;
	document.getElementById('cameraDrcHighId').checked = false;
	if (cameraDrc == "OFF")
	{
		document.getElementById('cameraDrcOffId').checked = true;
	}
	else if (cameraDrc == "LOW")
	{
		document.getElementById('cameraDrcLowId').checked = true;
	}
	else if (cameraDrc == "MEDIUM")
	{
		document.getElementById('cameraDrcMediumId').checked = true;
	}
	else if (cameraDrc == "HIGH")
	{
		document.getElementById('cameraDrcHighId').checked = true;
	}

	// Restore: cameraSharpness
	document.getElementById("cameraSharpnessId").value = cameraSharpness;
	document.getElementById("sharpnessRange").innerHTML = cameraSharpness;

	// Restore: cameraSharpness
	document.getElementById("cameraContrastId").value = cameraContrast;
	document.getElementById("contrastRange").innerHTML = cameraContrast;

	// Restore: cameraSharpness
	document.getElementById("cameraBrightnessId").value = cameraBrightness;
	document.getElementById("brightnessRange").innerHTML = cameraBrightness;

	// Restore: cameraSharpness
	document.getElementById("cameraSaturationId").value = cameraSaturation;
	document.getElementById("saturationRange").innerHTML = cameraSaturation;

	// Restore: cameraShutterSpeed
	document.getElementById("cameraShutterSpeedId").value = cameraShutterSpeed;
	document.getElementById("shutterSpeedRange").innerHTML = cameraShutterSpeed;
});

///
// Camera status management
///
function savePageStatus ()
{
	console.log("Data changed");

	if(typeof(Storage) !== "undefined")
	{
		// Debug
		console.log("Data saved");
	}
	else
	{
		// No Web Storage support
		console.log("No web support available on this browser");
	}
}

function handleCameraOnClick(cameraStatus)
{
	cameraStatus = cameraStatus.value;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraStatusLSItem', cameraStatus);
	}
}

function handleCameraOffClick(cameraStatus)
{
	cameraStatus = cameraStatus.value;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraStatusLSItem', cameraStatus);
	}
}

function handleCameraAutoClick(autoStatus)
{
	cameraAutoStatus = autoStatus.value;

    if (cameraAutoStatus == "AUTO")
    {
        document.getElementById('cameraAutoImgId').src = "/images/photo33.png";

        console.log ("Camera in AUTO mode");
    }
    else if (cameraAutoStatus == "MANUAL")
    {
        document.getElementById('cameraAutoImgId').src = "/images/users81.png";

        console.log ("Camera in MANUAL mode");
    }

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraAutoStatusLSItem', cameraAutoStatus);
	}
}

function handleCameraIsoClick(iso)
{
	cameraIso = iso.value;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraIsoLSItem', cameraIso);
	}

	console.log ("Camera ISO: " + cameraIso);
}

function handleCameraFlashOnClick(flash)
{
	cameraFlashStatus = flash.value;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraFlashStatusLSItem', cameraFlashStatus);
	}

    if (cameraFlashStatus == "AUTO")
    {
        document.getElementById('cameraFlashImgId').src = "/images/automatic5.png";
    }
    else if (cameraFlashStatus == "OFF")
    {
        document.getElementById('cameraFlashImgId').src = "/images/lightning10.png";
    }
	else if (cameraFlashStatus == "ON")
    {
        document.getElementById('cameraFlashImgId').src = "/images/flash29.png";
    }

	console.log ("Flash status: " + cameraFlashStatus);
}

function handleCameraSizeClick(size)
{
	cameraPictureSize = size.value;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraPictureSizeLSItem', cameraPictureSize);
	}

	console.log ("Picture size: " + cameraPictureSize);
}

function handleCameraExposureClick(exposure)
{
	cameraExposure = exposure.value;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraExposureLSItem', cameraExposure);
	}

    if (cameraExposure == "AUTO")
    {
		document.getElementById('cameraExposureImgId').src = "/images/camera69.png";
    }
    else if (cameraExposure == "NIGHT")
    {
        document.getElementById('cameraExposureImgId').src = "/images/camera70.png";
    }
    else if (cameraExposure == "SPOTLIGHT")
    {
        document.getElementById('cameraExposureImgId').src = "/images/camera65.png";
    }
    else if (cameraExposure == "VERYLONG")
    {
        document.getElementById('cameraExposureImgId').src = "/images/photo108.png";
    }

	console.log ("Exposure: " + cameraExposure);
}

function handleCameraWhiteBalanceClick(whiteBalance)
{
	cameraWhiteBalance = whiteBalance.value;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraWhiteBalanceLSItem', cameraWhiteBalance);
	}

	console.log ("White balance: " + cameraWhiteBalance);
}

function showSharpnessValue(newValue)
{
	cameraSharpness = newValue;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraSharpnessLSItem', cameraSharpness);
	}

	document.getElementById("sharpnessRange").innerHTML=newValue;
}

function showShutterSpeedValue(newValue)
{
	cameraShutterSpeed = newValue;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraShutterSpeedLSItem', cameraShutterSpeed);
	}

	document.getElementById("shutterSpeedRange").innerHTML=newValue;
}

function showContrastValue(newValue)
{
	cameraContrast = newValue;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraContrastLSItem', cameraContrast);
	}

	document.getElementById("contrastRange").innerHTML=newValue;
}

function showBrightnessValue(newValue)
{
	cameraBrightness = newValue;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraBrightnessLSItem', cameraBrightness);
	}

	document.getElementById("brightnessRange").innerHTML=newValue;
}

function showSaturationValue(newValue)
{
	cameraSaturation = newValue;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraSaturationLSItem', cameraSaturation);
	}

	document.getElementById("saturationRange").innerHTML=newValue;
}

function handleCameraDrcClick(drc)
{
	cameraDrc = drc.value;

	// Save data
	if(typeof(Storage) !== "undefined")
	{
		localStorage.setItem('cameraDrcLSItem', cameraDrc);
	}

	console.log ("Picture drc: " + cameraDrc);
}

///
// jQuery: used to take a picture on time interval
///
$(document).ready(function()
{
    // If we are in auto mode then take a picture
    if (autoIsSet == true)
    {
        job = setTimeout("TakePictureOnTimer(1)", 3000);
    }
});

var displayImage = function (base64Data) {
    //var imag = "<img "
    //         + "width='640'" + "height='480'" + "src='" + "data:image/jpg;base64,"
    //         + base64Data + "'/>";
    var imag = "<img "
             + "src='" + "data:image/jpg;base64,"
             + base64Data + "'/>";

	//console.log (base64Data);
	console.log (imag);

	if (base64Data)
	{
		$("#load_camera_image_div").html(imag)
	}
};

function StopAutoCapture()
{
    // Change auto mode status and button text
    if (autoIsSet == true)
    {
        replaceButtonText('my_custom_button', 'Start');
        autoIsSet = false;
        clearTimeout(job);

        console.log ("We are in manual mode");
    }
    else
    {
        replaceButtonText('my_custom_button', 'Stop');
        autoIsSet = true;

        job = setTimeout("TakePictureOnTimer(1)", 3000);

        console.log ("We are in auto mode");
    }
}

function replaceButtonText(buttonId, text)
{
  if (document.getElementById)
  {
    var button=document.getElementById(buttonId);
    if (button)
    {
      if (button.childNodes[0])
      {
        button.childNodes[0].nodeValue=text;
      }
      else if (button.value)
      {
        button.value=text;
      }
      else //if (button.innerHTML)
      {
        button.innerHTML=text;
      }
    }
  }
}

function Capture()
{
    // Take a picture
    console.log ("Take picture #" + pictureNumber);

    TakePictureOnTimer(pictureNumber);

    // Increment picture number
    pictureNumber++;
}

function TakePictureOnTimer(parameterValue)
{
    $.ajax({
        type: 'GET',
        url: 'display_camera_image.php',
        data: { get_param: parameterValue,
				cameraStatus_param: cameraStatus,
				cameraAutoStatus_param: cameraAutoStatus,
				cameraIso_param: cameraIso,
				cameraFlashStatus_param: cameraFlashStatus,
				cameraPictureSize_param: cameraPictureSize,
				cameraExposure_param: cameraExposure,
				cameraWhiteBalance_param: cameraWhiteBalance,
				cameraSharpness_param: cameraSharpness,
				cameraSaturation_param: cameraSaturation,
				cameraBrightness_param: cameraBrightness,
				cameraContrast_param: cameraContrast,
				cameraDrc_param: cameraDrc,
				cameraShutterSpeed_param: cameraShutterSpeed},
        dataType: 'json',
        success: function (data)
        {
            console.log (data['additionalData']);

            displayImage(data.base64picture);
        },
        error: function (data)
        {
            console.log ("Failed to load image");
            console.log (data['additionalData']);
        }
    });

    // We need to restart the timer to take another one
    if (autoIsSet == true)
    {
        job = setTimeout("TakePictureOnTimer(1)", 3000);
    }
}

// Create the tooltips only when document ready
$(function() {
    $('div#LargeWidthTip').qtip({
		content: {
			text: 'Camera image from remote'
		},
		position: {
			my: 'top center',  // Position my top left...
			at: 'bottom left', // at the bottom right of...
			target: 'mouse', // my target
			//adjust: { mouse: false }, // ...but don't follow the mouse
			adjust: {
				x: 10, y: 10
			}
		},
        style: {
            classes: 'ui-tooltip-content ui-tooltip-titlebar'
        },
		show: {
			target: $('h2:first'),
			delay: 1000
		}
    });
});
