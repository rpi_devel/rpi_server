///
// Variables
///
// Used to change color
var shouldRedirect1 = false;
var shouldRedirect2 = false;

// Variables used to pass parameters to the C application
var accGyro_fs_currentValue = 2;
var accGyro_odr_currentValue = 1600;
var accGyro_gyro_currentStatus = "OFF";
var console_currentStatus = "OFF";

// Variable used to link a function exetuted on timer, it is used to clear the timer
var console_refresh;

///
// On load event for the page we restore the session
///
$(document).ready(function()
{
	if(typeof(Storage) !== "undefined")
	{
		// Load the status for the TEMPERATURE/HUMIDITY switch
		if (localStorage.getItem("source_imageTemp") === null)
		{
		  // We did not previously save the local storage element
		}
		else
		{
			// Restore element status
			var switchStatus_imageTemp = document.getElementById('imageTemp');
			switchStatus_imageTemp.src = localStorage.getItem('source_imageTemp');
		}

		// Load the status for the ACC/GYRO switch
		if (localStorage.getItem("source_imageAccGyro") === null)
		{
		  // We did not previously save the local storage element
		}
		else
		{
			// Restore element status
			var switchStatus_imageAccGyro = document.getElementById('imageAccGyro');
			switchStatus_imageAccGyro.src = localStorage.getItem('source_imageAccGyro');
		}

		// Load the status for the ACC/GYRO selected type
		if (localStorage.getItem("checked_radio_LSM6DS0") === null &&
		    localStorage.getItem("checked_radio_LSM6DS3") === null)
		{
			var radioStatus_LSM6DS0 = document.getElementById('radio_LSM6DS0');
			radioStatus_LSM6DS0.checked = true;

			var radioStatus_LSM6DS3 = document.getElementById('radio_LSM6DS3');
			radioStatus_LSM6DS3.checked = true;
		}
		else
		{
			if (localStorage.getItem("checked_radio_LSM6DS0") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_LSM6DS0 = document.getElementById('radio_LSM6DS0');
				if (localStorage.getItem('checked_radio_LSM6DS0') == "true")
				{
					radioStatus_LSM6DS0.checked = true;
				}
				else
				{
					radioStatus_LSM6DS0.checked = false;
				}
			}

			if (localStorage.getItem("checked_radio_LSM6DS3") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_LSM6DS3 = document.getElementById('radio_LSM6DS3');
				if (localStorage.getItem('checked_radio_LSM6DS3') == "true")
				{
					radioStatus_LSM6DS3.checked = true;
				}
				else
				{
					radioStatus_LSM6DS3.checked = false;
				}
			}
		}

		// Load the status for the ACC/GYRO selected FS
		if (localStorage.getItem("checked_radio_accgyro_fs_2") === null &&
		    localStorage.getItem("checked_radio_accgyro_fs_4") === null &&
			localStorage.getItem("checked_radio_accgyro_fs_8") === null &&
			localStorage.getItem("checked_radio_accgyro_fs_16") === null)
		{
			var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_2');
			radioStatus_accgyro_fs.checked = true;

			var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_4');
			radioStatus_accgyro_fs.checked = false;

			var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_8');
			radioStatus_accgyro_fs.checked = false;

			var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_16');
			radioStatus_accgyro_fs.checked = false;
		}
		else
		{
			if (localStorage.getItem("checked_radio_accgyro_fs_2") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_2');
				if (localStorage.getItem('checked_radio_accgyro_fs_2') == "true")
				{
					radioStatus_accgyro_fs.checked = true;
				}
				else
				{
					radioStatus_accgyro_fs.checked = false;
				}
			}

			if (localStorage.getItem("checked_radio_accgyro_fs_4") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_4');
				if (localStorage.getItem('checked_radio_accgyro_fs_4') == "true")
				{
					radioStatus_accgyro_fs.checked = true;
				}
				else
				{
					radioStatus_accgyro_fs.checked = false;
				}
			}

			if (localStorage.getItem("checked_radio_accgyro_fs_8") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_8');
				if (localStorage.getItem('checked_radio_accgyro_fs_8') == "true")
				{
					radioStatus_accgyro_fs.checked = true;
				}
				else
				{
					radioStatus_accgyro_fs.checked = false;
				}
			}

			if (localStorage.getItem("checked_radio_accgyro_fs_16") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_16');
				if (localStorage.getItem('checked_radio_accgyro_fs_16') == "true")
				{
					radioStatus_accgyro_fs.checked = true;
				}
				else
				{
					radioStatus_accgyro_fs.checked = false;
				}
			}
		}

		// Load the status for the Acc/Gyro ODR
		if (localStorage.getItem("checked_radio_accgyro_odr_200") === null &&
		    localStorage.getItem("checked_radio_accgyro_odr_400") === null &&
			localStorage.getItem("checked_radio_accgyro_odr_800") === null &&
			localStorage.getItem("checked_radio_accgyro_odr_1600") === null)
		{
			var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_200');
			radioStatus_accgyro_odr.checked = true;

			var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_400');
			radioStatus_accgyro_odr.checked = false;

			var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_800');
			radioStatus_accgyro_odr.checked = false;

			var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_1600');
			radioStatus_accgyro_odr.checked = false;
		}
		else
		{
			if (localStorage.getItem("checked_radio_accgyro_odr_200") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_200');
				if (localStorage.getItem('checked_radio_accgyro_odr_200') == "true")
				{
					radioStatus_accgyro_odr.checked = true;
				}
				else
				{
					radioStatus_accgyro_odr.checked = false;
				}
			}

			if (localStorage.getItem("checked_radio_accgyro_odr_400") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_400');
				if (localStorage.getItem('checked_radio_accgyro_odr_400') == "true")
				{
					radioStatus_accgyro_odr.checked = true;
				}
				else
				{
					radioStatus_accgyro_odr.checked = false;
				}
			}

			if (localStorage.getItem("checked_radio_accgyro_odr_800") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_800');
				if (localStorage.getItem('checked_radio_accgyro_odr_800') == "true")
				{
					radioStatus_accgyro_odr.checked = true;
				}
				else
				{
					radioStatus_accgyro_odr.checked = false;
				}
			}

			if (localStorage.getItem("checked_radio_accgyro_odr_1600") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_1600');
				if (localStorage.getItem('checked_radio_accgyro_odr_1600') == "true")
				{
					radioStatus_accgyro_odr.checked = true;
				}
				else
				{
					radioStatus_accgyro_odr.checked = false;
				}
			}
		}

		// Load the status for the GYRO (OFF/ON)
		if (localStorage.getItem("checked_radio_accgyro_gyro_off") === null &&
		    localStorage.getItem("checked_radio_accgyro_gyro_on") === null)
		{
			var radioStatus_accgyro_gyro_status = document.getElementById('radio_accgyro_gyro_off');
			radioStatus_accgyro_gyro_status.checked = true;

			var radioStatus_accgyro_gyro_status = document.getElementById('radio_accgyro_gyro_on');
			radioStatus_accgyro_gyro_status.checked = false;
		}
		else
		{
			if (localStorage.getItem("checked_radio_accgyro_gyro_off") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_accgyro_gyro_status = document.getElementById('radio_accgyro_gyro_off');
				if (localStorage.getItem('checked_radio_accgyro_gyro_off') == "true")
				{
					radioStatus_accgyro_gyro_status.checked = true;
				}
				else
				{
					radioStatus_accgyro_gyro_status.checked = false;
				}
			}

			if (localStorage.getItem("checked_radio_accgyro_gyro_on") === null)
			{
			  // We did not previously save the local storage element
			}
			else
			{
				// Restore element status
				var radioStatus_accgyro_gyro_status = document.getElementById('radio_accgyro_gyro_on');
				if (localStorage.getItem('checked_radio_accgyro_gyro_on') == "true")
				{
					radioStatus_accgyro_gyro_status.checked = true;
				}
				else
				{
					radioStatus_accgyro_gyro_status.checked = false;
				}
			}
		}
	} else
	{
		// No Web Storage support
	}
});

///
// Event listener using HTML 5: used to load a file when the console is enabled
///
document.addEventListener('DOMContentLoaded', function ()
{
    var el = document.getElementById('radio_consoleOffId');

	if (el)
	{
	  el.addEventListener('click', eventConsoleStatusOffListener, false);
	}
});

document.addEventListener('DOMContentLoaded', function ()
{
	var el = document.getElementById('radio_consoleOnId');

	if (el)
	{
	  el.addEventListener('click', eventConsoleStatusOnListener, false);
	}
});

function eventConsoleStatusOnListener()
{
	console.log ("Console refresh on");

	console_refresh = setInterval(
	function ()
	{
		// Load the file in the text console
		//loadFileAsText();
		$.ajax({
	        type: 'GET',
	        url: 'download_console.php',
	        data: { get_param: 'console.txt' },
	        dataType: 'json',
	        success: function(data)
	        {
				var textArea = document.getElementById("inputTextToSave");
				textArea.value = data["text"];
				textArea.scrollTop = textArea.scrollHeight;

	            console.log ("Text area updated");
	        },
	        error: function (data)
	        {
	            console.log ("Failed to retrieve console");
	        }
	    });

		// Display the action
		//console.log ("Console refreshed");
	}, 1000); // refresh every 1000 milliseconds
}

function eventConsoleStatusOffListener()
{
	console.log ("Console refresh off");

	clearInterval(console_refresh);
}

///
// Event listener using HTML5: used to save the data on SAVE SESSION press and then restore switch image
///
document.addEventListener('DOMContentLoaded', function ()
{
    var el = document.getElementById('myImageLocalStorage');

	if (el)
	{
	  el.addEventListener('click', eventClickListener, false);
	}
});

///
// This function update the local storage in order to allow switches and radio
// boxes status if the page is reloaded or the browser is closed and page opened again
///
function eventClickListener()
{
    //document.getElementById('switches_text').innerHTML = "YOU CLICKED ME!";
	//localStorage.setItem('source', this.src);
	//localStorage.setItem('timestamp', (new Date()).getTime());

	console.log ("Event click listener called");

	// Save switch status inside local storage
	var switchStatus_imageTemp = document.getElementById('imageTemp');
	localStorage.setItem('source_imageTemp', switchStatus_imageTemp.src);
	localStorage.setItem('timestamp_imageTemp', (new Date()).getTime());

	var switchStatus_imageAccGyro = document.getElementById('imageAccGyro');
	localStorage.setItem('source_imageAccGyro', switchStatus_imageAccGyro.src);
	localStorage.setItem('timestamp_imageAccGyro', (new Date()).getTime());

	// Save Acc/Gyro sensor type
	var radioStatus_LSM6DS0 = document.getElementById('radio_LSM6DS0');
	if (radioStatus_LSM6DS0.checked == true)
	{
		localStorage.setItem('checked_radio_LSM6DS0', "true");
		localStorage.setItem('timestamp_radio_LSM6DS0', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_LSM6DS0', "false");
		localStorage.setItem('timestamp_radio_LSM6DS0', (new Date()).getTime());
	}

	var radioStatus_LSM6DS3 = document.getElementById('radio_LSM6DS3');
	if (radioStatus_LSM6DS3.checked == true)
	{
		localStorage.setItem('checked_radio_LSM6DS3', "true");
		localStorage.setItem('timestamp_radio_LSM6DS3', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_LSM6DS3', "false");
		localStorage.setItem('timestamp_radio_LSM6DS3', (new Date()).getTime());
	}

	// Save Acc/Gyro FS
	var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_2');
	if (radioStatus_accgyro_fs.checked == true)
	{
		localStorage.setItem('checked_radio_accgyro_fs_2', "true");
		localStorage.setItem('timestamp_radio_accgyro_fs_2', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_accgyro_fs_2', "false");
		localStorage.setItem('timestamp_radio_accgyro_fs_2', (new Date()).getTime());
	}

	var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_4');
	if (radioStatus_accgyro_fs.checked == true)
	{
		localStorage.setItem('checked_radio_accgyro_fs_4', "true");
		localStorage.setItem('timestamp_radio_accgyro_fs_4', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_accgyro_fs_4', "false");
		localStorage.setItem('timestamp_radio_accgyro_fs_4', (new Date()).getTime());
	}

	var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_8');
	if (radioStatus_accgyro_fs.checked == true)
	{
		localStorage.setItem('checked_radio_accgyro_fs_8', "true");
		localStorage.setItem('timestamp_radio_accgyro_fs_8', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_accgyro_fs_8', "false");
		localStorage.setItem('timestamp_radio_accgyro_fs_8', (new Date()).getTime());
	}

	var radioStatus_accgyro_fs = document.getElementById('radio_accgyro_fs_16');
	if (radioStatus_accgyro_fs.checked == true)
	{
		localStorage.setItem('checked_radio_accgyro_fs_16', "true");
		localStorage.setItem('timestamp_radio_accgyro_fs_2', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_accgyro_fs_16', "false");
		localStorage.setItem('timestamp_radio_accgyro_fs_16', (new Date()).getTime());
	}

	// Save Acc/Gyro ODR
	var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_200');
	if (radioStatus_accgyro_odr.checked == true)
	{
		localStorage.setItem('checked_radio_accgyro_odr_200', "true");
		localStorage.setItem('timestamp_radio_accgyro_odr_200', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_accgyro_odr_200', "false");
		localStorage.setItem('timestamp_radio_accgyro_odr_200', (new Date()).getTime());
	}

	var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_400');
	if (radioStatus_accgyro_odr.checked == true)
	{
		localStorage.setItem('checked_radio_accgyro_odr_400', "true");
		localStorage.setItem('timestamp_radio_accgyro_odr_400', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_accgyro_odr_400', "false");
		localStorage.setItem('timestamp_radio_accgyro_odr_400', (new Date()).getTime());
	}

	var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_800');
	if (radioStatus_accgyro_odr.checked == true)
	{
		localStorage.setItem('checked_radio_accgyro_odr_800', "true");
		localStorage.setItem('timestamp_radio_accgyro_odr_800', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_accgyro_odr_800', "false");
		localStorage.setItem('timestamp_radio_accgyro_odr_800', (new Date()).getTime());
	}

	var radioStatus_accgyro_odr = document.getElementById('radio_accgyro_odr_1600');
	if (radioStatus_accgyro_odr.checked == true)
	{
		localStorage.setItem('checked_radio_accgyro_odr_1600', "true");
		localStorage.setItem('timestamp_radio_accgyro_odr_1600', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_accgyro_odr_1600', "false");
		localStorage.setItem('timestamp_radio_accgyro_odr_1600', (new Date()).getTime());
	}
	var radioStatus_accgyro_gyro_status = document.getElementById('radio_accgyro_gyro_off');
	if (radioStatus_accgyro_gyro_status.checked == true)
	{
		localStorage.setItem('checked_radio_accgyro_gyro_off', "true");
		localStorage.setItem('timestamp_radio_accgyro_gyro_off', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_accgyro_gyro_off', "false");
		localStorage.setItem('timestamp_radio_accgyro_gyro_off', (new Date()).getTime());
	}

	var radioStatus_accgyro_gyro_status = document.getElementById('radio_accgyro_gyro_on');
	if (radioStatus_accgyro_gyro_status.checked == true)
	{
		localStorage.setItem('checked_radio_accgyro_gyro_on', "true");
		localStorage.setItem('timestamp_radio_accgyro_gyro_on', (new Date()).getTime());
	}
	else
	{
		localStorage.setItem('checked_radio_accgyro_gyro_on', "false");
		localStorage.setItem('timestamp_radio_accgyro_gyro_on', (new Date()).getTime());
	}
	// Fire a function to restore this switch to the previous status
	setInterval(function()
	{
		var image = document.getElementById('myImageLocalStorage');
		image.src = "images/switch_off.png";
	},1000);
}

///
// Server-event manager: it executes 'events.php' on server event, it is used to
// update the date and time informationon the page
///
if(typeof(EventSource) !== "undefined")
{
	// Define new server event for time and date
    var source = new EventSource("events.php");

    source.onmessage = function(event)
	{
		var elem = document.getElementById("eventResult");

		elem.value = event.data;
    };
	source.onerror = function(event)
	{
		//alert("EventSource failed: " + event.code);
	};
}
else
{
	// Set time and date field
	var elem = document.getElementById("eventResult");

    elem.value = "Sorry, your browser does not support server-sent events...";
}

///
// Server-event manager: it executes 'events.php' on server event
///
if(typeof(EventSource) !== "undefined")
{
	// Define new server event for application status
    var source = new EventSource("application_status.php");

    source.onmessage = function(event)
	{
		var elem = document.getElementById("statusResult");
		var str = event.data;

		if (null == str.match (/running/gi))
		{
			elem.style.color = "#FF0000"; // Red
		}
		else
		{
			elem.style.color = "#009900"; // Green
		}

		elem.value = event.data;
    };
	source.onerror = function(event)
	{
		//alert("EventSource failed: " + event.code);
	};
}
else
{
	// Set status field
	var elem = document.getElementById("statusResult");

    elem.value = "Sorry, your browser does not support server-sent events...";
}

///
// jQuery: executes record_count.php every second
///
$(document).ready(function(){
//	var imageIndex = "1";

	// Auto-refresh function for temperature
	var auto_refresh = setInterval(
	function ()
	{
		// Change the temperature if the feature has been activated
		var image = document.getElementById('imageTemp');

		if (image.src.match("switch_on"))
		{
			console.log ("Call record count");

            $('#load_temperature').load('../record_count.php').fadeIn("slow");
		}
		else
		{
			//console.log ("Record count not called becasue disabled");

			$('#load_temperature').text("Temperature report disabled");
		}
	}, 1000); // refresh every 1000 milliseconds

	var auto_refresh = setInterval(
	function ()
	{
		// Change the temperature if the feature has been activated
		var image = document.getElementById('imageAccGyro');

		if (image.src.match("switch_on"))
		{
			$('#load_accgyro').load('../accgyro_count.php').fadeIn("slow");
		}
		else
		{
			$('#load_accgyro').text("Acc/Gyro report disabled");
		}
	}, 500); // refresh every 500 milliseconds
});

///
// Actions to be executed on page load
///
function myOnloadFunc ()
{
	shouldRedirect1 = false;
	shouldRedirect2 = false;

	//var property1 = document.getElementById('button1');
	//property1.style.backgroundColor = "#0066CC";

	//var property2 = document.getElementById('button2');
	//property2.style.backgroundColor = "#0066CC";
}

///
// This function performs 2 different aciotns:
//    - It change the image of the switch
//    - It calls the php script that write data to the FIFO for the xternal program
///
function changeImage(elemId)
{
    var image = document.getElementById(elemId);

    if (image.src.match("switch_on"))
    {
		console.log ("Switch off");
        image.src = "images/switch_off.png";

		if (elemId == 'imageTemp')
		{
			$.ajax({
				url: '../write_fifo.php',
				type: "POST",
				data: ({myParam: "B"}),
				success: function(msg)
				{
					console.log ("Command: B");
				}
			});
		}
		else if (elemId == 'imageAccGyro')
		{
			$.ajax({
				url: '../write_fifo.php',
				type: "POST",
				data: ({myParam: "F"}),
				success: function(msg)
				{
					console.log ("Command: F");
				}
			});
		}
		else if (elemId == 'imageStartStopApp')
		{

		}
    }
	else
    {
		console.log ("Switch on");
        image.src = "images/switch_on.png";

		if (elemId == 'imageTemp')
		{
			$.ajax({
				url: '../write_fifo.php',
				type: "POST",
				data: ({myParam: "A"}),
				success: function(msg)
				{
					console.log ("Command: A");
				}
			});
		}
		else if (elemId == 'imageAccGyro')
		{
			// Check kind of sensor used
			var radioStatus_LSM6DS0 = document.getElementById('radio_LSM6DS0');

			if (radioStatus_LSM6DS0.checked == true)
			{
				$.ajax({
					url: '../write_fifo.php',
					type: "POST",
					dataType: "json",
					data: ({myParam: "E", accGyro_fs: accGyro_fs_currentValue, accGyro_odr: accGyro_odr_currentValue, accGyro_gyro_status: accGyro_gyro_currentStatus}),
					success: function(data)
					{
						console.log (data["cmd_to_send"]);
					},
					error: function(data)
					{
						console.log ("Error calling write_fifo.php")
					}
				});
			}
			else
			{
				$.ajax({
					url: '../write_fifo.php',
					type: "POST",
					dataType: "json",
					data: ({myParam: "G", accGyro_fs: accGyro_fs_currentValue, accGyro_odr: accGyro_odr_currentValue, accGyro_gyro_status: accGyro_gyro_currentStatus}),
					success: function(data)
					{
						console.log (data["cmd_to_send"]);
					},
					error: function(data)
					{
						console.log ("Error calling write_fifo.php")
					}
				});
			}
		}
		else if (elemId == 'imageStartStopApp')
		{
			$.ajax({
				url: '../write_fifo.php',
				type: "POST",
				data: ({myParam: "X"}),
				success: function(msg)
				{
					console.log ("Command: X");
				}
			});

			// Fire a function to restore this switch to the previous status
			setInterval(function()
			{
				var image = document.getElementById('imageStartStopApp');
				image.src = "images/switch_off.png";
			},1000);
		}
    }
}

///
// Functions used to save the current value for the radio boxes of the acc/gyro
///
function handleAccGyroFsClick(accGyroFsRadio)
{
	accGyro_fs_currentValue = accGyroFsRadio.value;

	console.log ("Acc/gyro FS current value: " + accGyro_fs_currentValue)
}

function handleAccGyroOdrClick(accGyroOdrRadio)
{
	accGyro_odr_currentValue = accGyroOdrRadio.value;

	console.log ("Acc/gyro ODR current value: " + accGyro_odr_currentValue)
}

///
// Functions executed on change state of the console radio button
///
function handleConsoleStatusClick(consoleStatus)
{
	console_currentStatus = consoleStatus.value;

	console.log ("Console current status: " + console_currentStatus)
}

function handleAccGyroGyroStatusClick(accGyroGyroStatus)
{
	accGyro_gyro_currentStatus = accGyroGyroStatus.value;

	console.log ("Acc/gyro ODR current value: " + accGyro_gyro_currentStatus)
}

///
// This function is used to change the color of custom buttons
///
function setColor(btn)
{
    var count=1;
    var property = document.getElementById(btn);

	if (btn == 'button1')
	{
		if (shouldRedirect1 == true)
		{
			property.style.backgroundColor = "#0066CC";
			property.style.color = "#FF3300";
			count=1;
			shouldRedirect1 = false;
		}
		else
		{
			property.style.backgroundColor = "#FF3300";
			property.style.color = "#0066CC";

			count=0;
			shouldRedirect1 = true;
		}
	}

	if (btn == 'button2')
	{
		if (shouldRedirect2 == true)
		{
			property.style.backgroundColor = "#0066CC";
			property.style.color = "#FF3300";
			count=1;
			shouldRedirect2 = false;
		}
		else
		{
			property.style.backgroundColor = "#FF3300";
			property.style.color = "#0066CC";

			count=0;
			shouldRedirect2 = true;
		}
	}
}

///
// Following functions are used to manage the console. They allow to load/save
// data in the text window dumping it and loading it for offline analysis
///
function saveTextAsFile()
{
	var textToWrite = document.getElementById("inputTextToSave").value;

	// Avoid to have '\n' stripped out from the saved file
	textToWrite = textToWrite.replace(/\n/g, "\r\n");
	var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
	var fileNameToSaveAs = document.getElementById("inputFileNameToSaveAs").value;
	var downloadLink = document.createElement("a");

	downloadLink.download = fileNameToSaveAs;
	downloadLink.innerHTML = "Download File";

	if (window.URL != null)
	{
		// Chrome allows the link to be clicked
		// without actually adding it to the DOM.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
	}
	else
	{
		// Firefox requires the link to be added to the DOM
		// before it can be clicked.
		downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
		downloadLink.onclick = destroyClickedElement;
		downloadLink.style.display = "none";
		document.body.appendChild(downloadLink);
	}

	downloadLink.click();
}

function destroyClickedElement(event)
{
	document.body.removeChild(event.target);
}

function loadFileAsText()
{
	var fileToLoad = document.getElementById("fileToLoad").files[0];
	var fileReader = new FileReader();

	fileReader.onload = function(fileLoadedEvent)
	{
		var textArea = document.getElementById("inputTextToSave");
		var textFromFileLoaded = fileLoadedEvent.target.result;

		textArea.value = textFromFileLoaded;
		textArea.scrolltop = textArea.scrollHeight;
	};

	fileReader.readAsText(fileToLoad, "UTF-8");
}

///
// These functions are used to download the files from the server, use of frames
// is done
///
function populateIframe1(id,path)
{
    var ifrm = document.getElementById(id);
    ifrm.src = "download.php?path="+path;
}

function populateIframe2(id,path)
{
    var ifrm = document.getElementById(id);
    ifrm.src = "download_temperature_humidity.php?path="+path;
}

///
// JS function to run the server-side php to run our application.
// The php script will kill and restart an external application
///
function runApplication()
{
    // Debug message
    console.log ("Running application from main...");

    // Call php to run application
    $.ajax({
        type: 'GET',
        url: 'run_application.php',
        data: { get_param: 'rpi_sensors' },
        dataType: 'json',
        success: function(data)
        {
            console.log (data.killedProc);
            console.log (data.newProc);

            console.log ("Run application with parameter: rpi_sensors");
        },
        error: function (data)
        {
            console.log ("Failed to run application");
        }
    });

    // Print weldone message
    console.log ("Done");
}
