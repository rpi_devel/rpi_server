// jQuery
$(document).ready(function(){
    var auto_refresh = setInterval(
    function ()
    {
        $.ajax({
            type: 'GET',
            url: 'display_image.php',
            data: { get_param: '1' },
            dataType: 'json',
            success: function (data) {
                displayImage(data.base64picture);
            },
            error: function (data) {
                console.log ("Failed to load image");
                //alert("Error loading image: " + data.responseText);
            }
        });
    }, 1500); // refresh every 'x' milliseconds
});

var displayImage = function (base64Data) {
    var imag = "<img "
             + "src='" + "data:image/png;base64,"
             + base64Data + "'/>";

	//console.log (base64Data);
	console.log (imag);

	if (base64Data)
	{
		$("#load_image_div").html(imag)
	}
};
