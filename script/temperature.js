// jQuery
$(document).ready(function(){
	var auto_refresh = setInterval(
	function ()
	{
		// Temperature sensor
		$('#load_temperature_page').load('../record_count.php').fadeIn("slow");
	}, 1000); // refresh every 1000 milliseconds
});	

// Create the tooltips only when document ready
$(function() {     
    $('div#LargeWidthTip').qtip({ 
		content: {
			text: 'Temperature and humidity samples if available are here'
		},
		position: {	
			my: 'top center',  // Position my top left...
			at: 'bottom left', // at the bottom right of...
			target: 'mouse', // my target
			//adjust: { mouse: false }, // ...but don't follow the mouse
			adjust: {
				x: 10, y: 10
			}
		},
        style: { 
            classes: 'ui-tooltip-content ui-tooltip-titlebar'
        },
		show: {
			target: $('h2:first'),
			delay: 1000
		}		
    });    
});