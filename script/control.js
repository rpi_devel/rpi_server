// JS function to run the server-side php to run our application
function runApplication() 
{
    // Debug message
    console.log ("Running application...");

    // Call php to run application
    $.ajax({
        type: 'GET', 
        url: 'run_application.php', 
        data: { get_param: 'rpi_sensors' }, 
        dataType: 'json',
        success: function(data) 
        {
            console.log (data.killedProc);
            console.log (data.newProc);

            console.log ("Run application with parameter: rpi_sensors");
        },        
        error: function (data) 
        {
            console.log ("Failed to run application");
        }        
    });   

    // Print weldone message
    console.log ("Done");  
}

