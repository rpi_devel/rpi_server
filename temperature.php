<!DOCTYPE html PUBLIC "-/
/W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="script/jquery-2.1.4.min.js"></script>
<script src="script/jquery.qtip.min.js"></script>
<script src="script/temperature.js"></script>
<link rel="stylesheet" type="text/css" href="style/jquery.qtip.min.css" />
<link rel="stylesheet" type="text/css" href="style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<div class="wrapper">
<?php
// Set timezone to avoid date() warning message
date_default_timezone_set('Europe/Rome');
?>

<nav>
  <ul>
    <li>
        <a href="temperature_humidity.php">Temperature Page</a>
    </li>
    <li>
        <a href="accgyro.php">Acc/Gyro Page</a>
    </li>
    <li>
        <a href="control.php">Control</a>
    </li>
    <li>
        <a href="camera.php">Camera</a>
    </li>
    <li>
        <a href="index.php">Home</a>
    </li>
  </ul>
</nav>

<div id="LargeWidthTip"> <h2>Temperature Page</h2></div>
<div id="load_temperature_page" class="text_user_message">Temperature sensor report...</div>
</div>
<div class="push"></div>

<div id="footer" class="footer_class">
	<p>Remote Control Site, &copy; 2015-<? echo date("Y")?> Our srl</p>
</div>
</body>
</html>