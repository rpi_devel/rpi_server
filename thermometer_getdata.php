<?php
$usrParam = $_POST['myParam'];

//if ($usrParam < 50)
//{
//	$usrParam = $usrParam + 1;
//}
//else
//{
//	$usrParam = -10;
//}

$cursor = -1;
$previousLine = $fileLine = "";

// Open the application report file and get size
$myfile = fopen("./logs/application_temperature.txt", "r") or die("Unable to open file!");

$fileSize = filesize("./logs/application_temperature.txt");

//fseek($myfile, $cursor, SEEK_END);

while(!feof($myfile))
{
	$previousLine = $fileLine;
	$fileLine = fgets($myfile, 1024);
}

if ($previousLine == "" || $previousLine == "\n")
{
	$previousLine = "Waiting for data...";
}

// Output line
$previousLine = mb_convert_encoding($previousLine,"UTF-8","ISO-8859-1");

//echo $previousLine;

fclose($myfile);

$data = array('temperature'=>"30",
			  'humidity'=>"48",
			  'text_temperature_humidity'=>$previousLine,
             );
print json_encode($data);
?>
