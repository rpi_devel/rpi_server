<!DOCTYPE html PUBLIC "-/
/W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="script/jquery-2.1.4.min.js"></script>
<script src="script/jquery.qtip.min.js"></script>
<script src="script/camera.js"></script>
<link rel="stylesheet" type="text/css" href="style/jquery.qtip.min.css" />
<link rel="stylesheet" type="text/css" href="style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
input[type=radio] {
    transform: scale(2);
	float: left;
    margin-left: -20px;
}
</style>
</head>

<body class="page-id-002">
<?php
// Set timezone to avoid date() warning message
date_default_timezone_set('Europe/Rome');
?>
<div class="wrapper">
    <nav>
      <ul>
        <li>
            <a href="temperature_humidity.php">Temperature Page</a>
        </li>
        <li>
            <a href="accgyro.php">Acc/Gyro Page</a>
        </li>
        <li>
            <a href="control.php">Control</a>
        </li>
        <li>
            <a href="camera.php">Camera</a>
        </li>
        <li>
            <a href="switch.php">Switches</a>
        </li>        
        <li>
            <a href="index.php">Home</a>
        </li>
      </ul>
    </nav>

    <div id="LargeWidthTip"> <h2>Camera Page</h2></div>
    <br>
    <p><input type='submit' value='Start' id='my_custom_button' onclick="StopAutoCapture()"/>Press to stop/start auto capture on timer</p>
    <br>
    <p><input type='submit' value='Capture' id='my_custom_button' onclick="Capture()"/>Take a picture</p>
    <br>
    <div id="load_camera_image_div">
        <img name="load_camera_image_name" id="load_camera_image" alt="Camera image" class="text_user_message">
    </div>

<div id="cameraControlDivId">
    <h2>Camera settings</h2>
    <table width="820px" border="0px">
        <col width="100px"/>
        <col width="120px"/>
        <col width="120px"/>
        <col width="120px"/>
        <col width="120px"/>
        <col width="120px"/>
        <col width="120px"/>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraImageClass" id="cameraImageImgId" src="images/photography52.png" width="48" height="48">
            </td>
            <td colspan="2" height="80">
                <input type="radio" id="cameraOnId" name="cameraStatus" onclick="handleCameraOnClick(this);" value="ON">CAMERA ON
            </td>
            <td colspan="2" height="80">
        	    <input type="radio" id="cameraOffId" name="cameraStatus" onclick="handleCameraOffClick(this);" value="OFF">CAMERA OFF
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraAutoClass" id="cameraAutoImgId" src="images/photo33.png" width="48" height="48">
            </td>
            <td colspan="2" height="80">
                <input type="radio" id="cameraAutoAutoId" name="cameraAuto" checked onclick="handleCameraAutoClick(this);" value="AUTO">Auto mode
            </td>
            <td colspan="2" height="80">
                <input type="radio" id="cameraAutoManualId" name="cameraAuto" onclick="handleCameraAutoClick(this);" value="MANUAL">Manual mode
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraIsoClass" id="cameraIsoImgId" src="images/iso10.png" width="48" height="48">
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraIso100Id" name="cameraIso" checked onclick="handleCameraIsoClick(this);" value="100">100
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraIso200Id" name="cameraIso" onclick="handleCameraIsoClick(this);" value="200">200
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraIso400Id" name="cameraIso" onclick="handleCameraIsoClick(this);" value="400">400
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraIso800Id" name="cameraIso" onclick="handleCameraIsoClick(this);" value="800">800
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraFlashClass" id="cameraFlashImgId" src="images/automatic5.png" width="48" height="48">
            </td>
            <td colspan="2" height="80">
                <input type="radio" id="cameraFlashAutoId" name="cameraFlash" checked onclick="handleCameraFlashOnClick(this);" value="AUTO">Flash auto
            </td>
            <td colspan="2" height="80">
                <input type="radio" id="cameraFlashOffId" name="cameraFlash" onclick="handleCameraFlashOnClick(this);" value="OFF">Flash off
            </td>
            <td colspan="2" height="80">
                <input type="radio" id="cameraFlashOnId" name="cameraFlash" onclick="handleCameraFlashOnClick(this);" value="ON">Flash on
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraSizeClass" id="cameraSizeImgId" src="images/dimensions.png" width="48" height="48">
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraSizeAutoId" name="cameraSize" checked onclick="handleCameraSizeClick(this);" value="AUTO">Auto<br>
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraSize2592x1944Id" name="cameraSize" onclick="handleCameraSizeClick(this);" value="2592x1944">2592x1944<br>4:3
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraSize1920x1080Id" name="cameraSize" onclick="handleCameraSizeClick(this);" value="1920x1080">1920x1080<br>16:9
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraSize1296x972Id" name="cameraSize" onclick="handleCameraSizeClick(this);" value="1296x972">1296x972<br>4:3
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraSize1296x730Id" name="cameraSize" onclick="handleCameraSizeClick(this);" value="1296x730">1296x730<br>16:9
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraSize640x480Id" name="cameraSize" onclick="handleCameraSizeClick(this);" value="640x480">640x480<br>4:3
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraExposureClass" id="cameraExposureImgId" src="images/camera69.png" width="48" height="48">
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraExposureAutoId" name="cameraExposure" checked onclick="handleCameraExposureClick(this);" value="AUTO">Auto
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraExposureNightId" name="cameraExposure" onclick="handleCameraExposureClick(this);" value="NIGHT">Night
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraExposureVeryLongId" name="cameraExposure" onclick="handleCameraExposureClick(this);" value="VERYLONG">Very long
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraExposureSpotLightId" name="cameraExposure" onclick="handleCameraExposureClick(this);" value="SPOTLIGHT">Spotlight
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraSharpnessClass" id="cameraSharpnessImgId" src="images/camera63.png" width="48" height="48">
            </td>
            <td colspan="4" height="80">
                <input type="range" id="cameraSharpnessId" class="sliderCamera" min="-100" max="100" value="0" step="10" onchange="showSharpnessValue(this.value)" />
            </td>
            <td colspan="1" height="80">
                <span id="sharpnessRange">0</span>&nbsp;&nbsp;&nbsp;Sharpness
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraContrastClass" id="cameraContrastImgId" src="images/contrast25.png" width="48" height="48">
            </td>
            <td colspan="4" height="80">
                <input type="range" id="cameraContrastId" class="sliderCamera" min="-100" max="100" value="0" step="10" onchange="showContrastValue(this.value)" />
            </td>
            <td colspan="1" height="80">
                <span id="contrastRange">0</span>&nbsp;&nbsp;&nbsp;Contrast
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraBrightnessClass" id="cameraBrightnessImgId" src="images/brightness14.png" width="48" height="48">
            </td>
            <td colspan="4" height="80">
                <input type="range" id="cameraBrightnessId" class="sliderCamera" min="0" max="100" value="0" step="10" onchange="showBrightnessValue(this.value)" />
            </td>
            <td colspan="1" height="80">
                <span id="brightnessRange">0</span>&nbsp;&nbsp;&nbsp;Brightness
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraSaturationClass" id="cameraSaturationImgId" src="images/photography54.png" width="48" height="48">
            </td>
            <td colspan="4" height="80">
                <input type="range" id="cameraSaturationId" class="sliderCamera" min="-100" max="100" value="0" step="10" onchange="showSaturationValue(this.value)" />
            </td>
            <td colspan="1" height="80">
                <span id="saturationRange">0</span>&nbsp;&nbsp;&nbsp;Saturation
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraWhiteBalanceClass" id="cameraWhiteBalanceImgId" src="images/auto1.png" width="48" height="48">
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraWhiteBalanceAutoId" name="cameraWhiteBalance" checked onclick="handleCameraWhiteBalanceClick(this);" value="AUTO">Auto
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraWhiteBalanceOffId" name="cameraWhiteBalance" onclick="handleCameraWhiteBalanceClick(this);" value="OFF">Off
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraWhiteBalanceSunnyId" name="cameraWhiteBalance" onclick="handleCameraWhiteBalanceClick(this);" value="SUNNY">Sunny
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraWhiteBalanceFlashId" name="cameraWhiteBalance" onclick="handleCameraWhiteBalanceClick(this);" value="FLASH">Flash
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraWhiteBalanceShadeId" name="cameraWhiteBalance" onclick="handleCameraWhiteBalanceClick(this);" value="SHADE">Shade
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraWhiteBalanceCloudyId" name="cameraWhiteBalance" onclick="handleCameraWhiteBalanceClick(this);" value="CLOUDY">Cloudy
            </td>
        </tr>
        <tr>
            <td colspan="1" height="80">
                <img class="cameraDrcClass" id="cameraDrcImgId" src="images/automatic4.png" width="48" height="48">
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraDrcOffId" name="cameraDrc" checked onclick="handleCameraDrcClick(this);" value="OFF">Drc Off
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraDrcLowId" name="cameraDrc" onclick="handleCameraDrcClick(this);" value="LOW">Drc Low
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraDrcMediumId" name="cameraDrc" onclick="handleCameraDrcClick(this);" value="MEDIUM">Drc Medium
            </td>
            <td colspan="1" height="80">
                <input type="radio" id="cameraDrcHighId" name="cameraDrc" onclick="handleCameraDrcClick(this);" value="HIGH">Drc High
            </td>
        </tr>
    <tr>
        <td colspan="1" height="80">
            <img class="cameraShutterSpeedClass" id="cameraShutterSpeedImgId" src="images/photocamera27.png" width="48" height="48">
        </td>
        <td colspan="4" height="80">
            <input type="range" id="cameraShutterSpeedId" class="sliderCamera" min="1000" max="25000" value="4000" step="1000" onchange="showShutterSpeedValue(this.value)" />
        </td>
        <td colspan="1" height="80">
            <span id="shutterSpeedRange">1000</span>&nbsp;&nbsp;ShutterSpeed
        </td>
    </tr>
    </table>
</div>
</div> <!-- Class wrapper -->

<div class="push"></div>

<div id="footer" class="footer_class">
	<p>Remote Control Site, &copy; 2015-<? echo date("Y")?> Our srl</p>
</div>
</body>
</html>
