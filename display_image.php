<?php
// Get params
$value = $_GET['get_param'];

if ($value == "1")
{
	$filename = "images/gnuplot_output.png";
    //$image = file_get_contents("./images/gnuplot_output.png");     
}
else
{
	$filename = "images/file_browse_pressed.png";
    //$image = file_get_contents("./images/file_browse_pressed.png");              
}

$fileSize = filesize($filename);
if ($fileSize == 0)
{
	// We do not have a valid file just return
	exit ();
}

$image = file_get_contents($filename);  
if($image === FALSE) 
{ 
	// We do not have a valid file just return
	exit ();
}
           
$base64 = base64_encode($image);
$title = "Log displayed: " . $value;
$additionalData = "Additional data";

$data = array('base64picture'=>$base64,
              'title'=>$title,
              'additionalData'=>$additionalData,
             );

print json_encode($data);
?>