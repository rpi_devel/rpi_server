<!DOCTYPE html PUBLIC "-/
/W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="script/jquery-2.1.4.min.js"></script>
<script src="script/control.js"></script>
<link rel="stylesheet" type="text/css" href="style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>

<body>
<div class="wrapper">
<?php
// Set timezone to avoid date() warning message
date_default_timezone_set('Europe/Rome');
?>

<nav>
  <ul>
    <li>
        <a href="temperature_humidity.php">Temperature Page</a>
    </li>
    <li>
        <a href="accgyro.php">Acc/Gyro Page</a>
    </li>
    <li>
        <a href="control.php">Control</a>
    </li>
    <li>
        <a href="camera.php">Camera</a>
    </li>
    <li>
        <a href="switch.php">Switches</a>
    </li>    
    <li>
        <a href="index.php">Home</a>
    </li>
  </ul>
</nav>

<div> <h2>Raspberry application control</h2></div>
<p><input type='submit' value='' id='app_start_stop_button' onclick="runApplication()"/></p>
<div> <h2>Raspberry Pi B Layout and J8 connector</h2></div>
<br>
<div style="position:absolute;left:150px;" class="left_image">
<img id="imageRPi" name="imageRPiTop_name" src="images/Raspberry_Pi_B+_top.jpg" width="1004" height="673">
</div>

<div style="position:absolute;left:1150px;" class="right_image">
<img id="imageRPi" name="imageRPiGpioLayout_name" src="images/Raspberry-Pi-GPIO-Layout-Model-B-Plus.png" width="204" height="670">
</div>
</div>
<div class="push"></div>
<div id="footer" class="footer_class">
	<p>Remote Control Site, &copy; 2015-<? echo date("Y")?> Our srl</p>
</div>
</body>
</html>
