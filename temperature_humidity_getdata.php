<?php
$usrParam = $_POST['myParam'];

$fileLine = "";
$previousLine = "";

// Open the application report file and get size
$myfile = fopen("logs/application_temperature_lastread.txt", "r") or die("Unable to open file!");

//$fileSize = filesize("logs/application_temperature.txt");
//fseek($myfile, -52, SEEK_END);

while(!feof($myfile))
{
	$previousLine = $fileLine;
	$fileLine = fgets($myfile, 1024);
}

if ($fileLine == "" || $fileLine == "\n")
{
	if ($previousLine == "" || $previousLine == "\n")
	{
		$fileLine = "Waiting for data...";
	}
	else
	{
		$fileLine = $previousLine;
	}
}

// Separate string part to get the numbers
$posFirstDelimiter = strpos($fileLine, ':', 0);
$posSecondDelimiter = strpos($fileLine, '.', $posFirstDelimiter);
$subStrTemp = substr($fileLine, ($posFirstDelimiter + 2), ($posSecondDelimiter - $posFirstDelimiter - 2));

$posFirstDelimiter = strpos($fileLine, ':', ($posFirstDelimiter + 1));
$posSecondDelimiter = strpos($fileLine, '.', $posFirstDelimiter);
$subStrHumidity = substr($fileLine, ($posFirstDelimiter + 2), ($posSecondDelimiter - $posFirstDelimiter - 2));

// Output line
$fileLine = mb_convert_encoding($fileLine,"UTF-8","ISO-8859-1");

fclose($myfile);

$data = array('temperature'=>$subStrTemp,
			  'humidity'=>$subStrHumidity,
			  'text_temperature_humidity'=>$fileLine,
             );
print json_encode($data);
?>
