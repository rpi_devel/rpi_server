<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="script/jquery-2.1.4.min.js"></script>
<script src="script/script.js"></script>
<link rel="stylesheet" type="text/css" href="style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body onload="myOnloadFunc();">
<div class="wrapper">
<?php
// Set timezone to avoid date() warning message
date_default_timezone_set('Europe/Rome');

// Helper function
function debug_to_console ($data)
{
    if (is_array( $data ))
	{
        $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
	}
    else
	{
        $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";
	}

    echo $output;
}
// define variables and set to empty values
$directionErr = $direction = $fileNameSave = "";
$consoleStatus = "OFF";

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	if (empty($_POST["direction"]))
	{
		$directionErr = "direction is required";
	}
	else
	{
		$direction = test_input($_POST["direction"]);
	}

	if (empty($_POST["inputFileNameToSaveAs"]))
	{
		$fileNameSave = "";
	}
	else
	{
		$fileNameSave = test_input($_POST["inputFileNameToSaveAs"]);
	}
}

function remove_file_bom($text)
{
    $clean = preg_replace('/^ /', '', $text);

    return $clean;
}

function test_input($data)
{
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);

	return $data;
}
?>

<nav>
  <ul>
    <li>
        <a href="temperature_humidity.php">Temperature Page</a>
    </li>
    <li>
        <a href="accgyro.php">Acc/Gyro Page</a>
    </li>
    <li>
        <a href="control.php">Control</a>
    </li>
    <li>
        <a href="camera.php">Camera</a>
    </li>
    <li>
        <a href="switch.php">Switches</a>
    </li>
    <li>
        <a href="index.php">Home</a>
    </li>
  </ul>
</nav>

<div> <h1><b>Remote Control Page</b></h3></div>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<div id="load_appexit" class="text_user_message">Application is running</div>
<div id="load_temperature" class="text_user_message">Temperature sensor report...</div>
<div id="load_accgyro" class="text_user_message">Accelerometer/Gyroscope sensor report...</div>
<div id="main">
	<h3>Server Time and Date</h3>
		<div id="eventDiv">
		<input type="text" id="eventResult" size="85" readonly>
		</div>
	<h3>Sensor log files download</h3>
		<iframe id="frame1" style="display:none"></iframe>
		<a href="javascript:populateIframe1('frame1','<?php echo $path; ?>')">Download accelerometer/gyroscope data file</a>
		<br><br>
		<iframe id="frame2" style="display:none"></iframe>
		<a href="javascript:populateIframe2('frame2','<?php echo $path; ?>')">Download temperature/humidity data file</a>
	<h3>Application stop/start (forced)</h3>
		<div id="load_image_div">
		<p><input type='submit' value='' id='app_start_stop_button' onclick="runApplication()"/></p>
		</div>
		<div id="statusDiv">
		<input type="text" id="statusResult" size="85" readonly>
		</div>
	<h3>Enable buttons</h3>
		   <p><div>
			<!-- Temperature switch -->
			<a class="tooltip">
			    <img class="tooltip" id="imageTemp" onclick="changeImage('imageTemp')" src="images/switch_off.png" width="80" height="110">
			    <span>
			        <img class="callout" src="images/callout.gif" />
			        Switch temperature sensor
			    </span>
			</a>
			<!-- Accelerometer/Gyroscope switch -->
			<a class="tooltip">
			    <img class="tooltip" id="imageAccGyro" onclick="changeImage('imageAccGyro')" src="images/switch_off.png" width="80" height="110">
		   		<span>
			        <img class="callout" src="images/callout.gif" />
			        Switch acc/gyro sensor
			    </span>
			</a>

			<!-- Start/Stop application switch -->
			<a class="tooltip">
			    <img class="tooltip" id="imageStartStopApp" onclick="changeImage('imageStartStopApp')" src="images/switch_off.png" width="80" height="110">
		   		<span>
			        <img class="callout" src="images/callout.gif" />
			        Exit application
			    </span>
			</a>

			<!-- Save session in local storage -->
			<a class="tooltip">
				<img id="myImageLocalStorage" onclick="changeImage('myImageLocalStorage')" src="images/switch_off.png" width="80" height="110">
		   		<span>
			        <img class="callout" src="images/callout.gif" />
			        Local storage save session
			    </span>
			</a>
		   </p></div>
		<div id="switches_text"> <p>Enable: Temperature, Accelerometer/Gyroscope, Exit application, Save session</p></div>
	    <b>Acc/Gyro sensor: </b>[* Choice will persist over sessions]
		<br><br>
		<div>
	    <tab><input type="radio" id="radio_LSM6DS0" name="direction" checked <?php if (isset($direction) && $direction=="LSM6DS0") echo "checked";?>  value="Board control">LSM6DS0
	    <input type="radio" id="radio_LSM6DS3" name="direction" <?php if (isset($direction) && $direction=="LSM6DS3") echo "checked";?>  value="Data read">LSM6DS3
		</div>
		<br>
		<div>
	    <tab><input type="radio" id="radio_accgyro_fs_2" name="accgyro_fs" onclick="handleAccGyroFsClick(this);" value="2" checked <?php if (isset($accgyro_fs) && $accgyro_fs=="ACCGYRO_FS_4") echo "checked";?>  value="Board control">FS=+/-2
	    <input type="radio" id="radio_accgyro_fs_4" name="accgyro_fs" onclick="handleAccGyroFsClick(this);" value="4" <?php if (isset($accgyro_fs) && $accgyro_fs=="ACCGYRO_FS_4") echo "checked";?>  value="Data read">FS=+/-4
	    <input type="radio" id="radio_accgyro_fs_8" name="accgyro_fs" onclick="handleAccGyroFsClick(this);" value="8" <?php if (isset($accgyro_fs) && $accgyro_fs=="ACCGYRO_FS_8") echo "checked";?>  value="Data read">FS=+/-8
	    <input type="radio" id="radio_accgyro_fs_16" name="accgyro_fs" onclick="handleAccGyroFsClick(this);" value="16" <?php if (isset($accgyro_fs) && $accgyro_fs=="ACCGYRO_FS_16") echo "checked";?>  value="Data read">FS=+/-16
		</div>
		<br>
		<div>
	    <tab><input type="radio" id="radio_accgyro_odr_200" name="accgyro_odr" onclick="handleAccGyroOdrClick(this);" value="200" <?php if (isset($accgyro_odr) && $accgyro_odr=="ACCGYRO_ODR_200") echo "checked";?>  value="Board control">ODR=200
	    <input type="radio" id="radio_accgyro_odr_400" name="accgyro_odr" onclick="handleAccGyroOdrClick(this);" value="400" <?php if (isset($accgyro_odr) && $accgyro_odr=="ACCGYRO_ODR_400") echo "checked";?>  value="Data read">ODR=400
	    <input type="radio" id="radio_accgyro_odr_800" name="accgyro_odr" onclick="handleAccGyroOdrClick(this);" value="800" <?php if (isset($accgyro_odr) && $accgyro_odr=="ACCGYRO_ODR_800") echo "checked";?>  value="Data read">ODR=800
	    <input type="radio" id="radio_accgyro_odr_1600" name="accgyro_odr" onclick="handleAccGyroOdrClick(this);" value="1600" checked <?php if (isset($accgyro_odr) && $accgyro_odr=="ACCGYRO_ODR_1600") echo "checked";?>  value="Data read">ODR=1600
		</div>
		<br>
		<div>
	    <tab><input type="radio" id="radio_accgyro_gyro_off" name="accgyro_gyro_status" onclick="handleAccGyroGyroStatusClick(this);" value="OFF" <?php if (isset($accgyro_gyro_status) && $accgyro_gyro_status=="ACCGYRO_GYRO_OFF") echo "checked";?>  value="Board control">GYRO=OFF
	    <input type="radio" id="radio_accgyro_gyro_on" name="accgyro_gyro_status" onclick="handleAccGyroGyroStatusClick(this);" value="ON" <?php if (isset($accgyro_gyro_status) && $accgyro_gyro_status=="ACCGYRO_GYRO_ON") echo "checked";?>  value="Data read">GYRO=ON
		</div>
		<br>
	<h3>Debug</h3>
		<p>
			<div>
			<b>Enable console: </b>
		    <tab><input type="radio" id="radio_consoleOffId" name="console" checked onclick="handleConsoleStatusClick(this);" value="OFF">OFF
		    <input type="radio" id="radio_consoleOnId" name="console" onclick="handleConsoleStatusClick(this);" value="ON">ON
			</div>
	    </p>
		<br>
		<table>
			<tr><td>Console</td></tr>
			<tr>
				<td colspan="3">
					<textarea id="inputTextToSave" style="width:640px;height:256px"></textarea>
				</td>
			</tr>

			<tr>
				<td>Filename to Save As:</td>
				<td><input id="inputFileNameToSaveAs"></input></td>
				<td><button type='button' onclick="saveTextAsFile()">Save Text to File</button></td>
			</tr>
			<tr>
				<td>Select a File to Load:</td>
				<td><input type="file" id="fileToLoad"></td>
				<td><button type='button' onclick="loadFileAsText()">Load Selected File</button><td>
			</tr>
		</table>
</div>
</form>

</div>
<div class="push"></div>

<div id="footer" class="footer_class">
	<p>Remote Control Site, &copy; 2015-<? echo date("Y")?> Our srl</p>
</div>
</body>
</html>
