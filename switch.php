<!DOCTYPE html PUBLIC "-/
/W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="script/jquery-2.1.4.min.js"></script>
<script src="script/jquery.qtip.min.js"></script>
<script src="script/switch.js"></script>
<link rel="stylesheet" type="text/css" href="style/jquery.qtip.min.css" />
<link rel="stylesheet" type="text/css" href="style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
input[type=radio] {
    transform: scale(2);
	float: left;
    margin-left: -20px;
}
</style>
</head>

<body class="page-id-002">
<?php
// Set timezone to avoid date() warning message
date_default_timezone_set('Europe/Rome');
?>
<div class="wrapper">
    <nav>
      <ul>
        <li>
            <a href="temperature_humidity.php">Temperature Page</a>
        </li>
        <li>
            <a href="accgyro.php">Acc/Gyro Page</a>
        </li>
        <li>
            <a href="control.php">Control</a>
        </li>
        <li>
            <a href="camera.php">Camera</a>
        </li>
        <li>
            <a href="switch.php">Switches</a>
        </li>
        <li>
            <a href="index.php">Home</a>
        </li>
      </ul>
    </nav>

	<div id="cameraControlDivId">
	    <h2>Switches status</h2>
	    <table border="0px">
	        <col width="80px"/>
	        <col width="150px"/>
	        <col width="250px"/>
	        <tr>
	            <td colspan="0" height="80">
	                <img class="switchControlCh1ImageClass" id="switchControlCh1ImageImgId" onclick="changeImage('switchControlCh1ImageImgId')" src="images/switch_on.png" width="80" height="110">
	            </td>
				<td colspan="1" height="80">
	                <img class="switchStatusCh1ImageClass" id="switchStatusCh1ImageImgId" src="images/electrical_plug_lime.png" width="150" height="150">
	            </td>
				<td colspan="2" height="80">
					<div id="switchCh1Text"> <p>Switch channel 1</p></div>
				</td>
	        </tr>
			<tr>
				<td colspan="0" height="80">
					<img class="switchControlCh2ImageClass" id="switchControlCh2ImageImgId" onclick="changeImage('switchControlCh2ImageImgId')" src="images/switch_on.png" width="80" height="110">
				</td>
				<td colspan="1" height="80">
					<img class="switchStatusCh2ImageClass" id="switchStatusCh2ImageImgId" src="images/electrical_plug_lime.png" width="150" height="150">
				</td>
				<td colspan="2" height="80">
					<div id="switchCh2Text"> <p>Switch channel 2</p></div>
				</td>
			</tr>
			<tr>
				<td colspan="0" height="80">
					<img class="switchControlCh3ImageClass" id="switchControlCh3ImageImgId" onclick="changeImage('switchControlCh3ImageImgId')" src="images/switch_on.png" width="80" height="110">
				</td>
				<td colspan="1" height="80">
					<img class="switchStatusCh3ImageClass" id="switchStatusCh3ImageImgId" onclick="changeImage('switchControlCh4ImageImgId')" src="images/electrical_plug_lime.png" width="150" height="150">
				</td>
				<td colspan="2" height="80">
					<div id="switchCh3Text"> <p>Switch channel 3</p></div>
				</td>
			</tr>
			<tr>
				<td colspan="0" height="80">
					<img class="switchControlCh4ImageClass" id="switchControlCh4ImageImgId" onclick="changeImage('switchControlCh4ImageImgId')" src="images/switch_on.png" width="80" height="110">
				</td>
				<td colspan="1" height="80">
					<img class="switchStatusCh4ImageClass" id="switchStatusCh4ImageImgId" src="images/electrical_plug_lime.png" width="150" height="150">
				</td>
				<td colspan="2" height="80">
					<div id="switchCh4Text"> <p>Switch channel 4</p></div>
				</td>
			</tr>
	    </table>
	</div>
</div> <!-- Class wrapper -->

<div class="push"></div>

<div id="footer" class="footer_class">
	<p>Remote Control Site, &copy; 2015-<? echo date("Y")?> Our srl</p>
</div>
</body>
</html>
