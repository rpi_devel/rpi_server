<?php
$usrParam = $_POST['myParam'];

$fileLine = "";
$previousLine = "";

$channelNumber = 1;
$gpioNumber = 23;

$switchStatusArray = array (
    array ("channel" => "1", "gpio" => "UNDEF", "status"  => "UNKNOWN"),
    array ("channel" => "2", "gpio" => "UNDEF", "status"  => "UNKNOWN"),
    array ("channel" => "3", "gpio" => "UNDEF", "status"  => "UNKNOWN"),
    array ("channel" => "4", "gpio" => "UNDEF", "status"  => "UNKNOWN")
);

// Open the application report file and get size
$myfile = fopen("logs/switch_status.txt", "r") or die("Unable to open file!");

// Check last time the file was modified, if it is too old data is not considered
// usable
$fileTime = filemtime("logs/switch_status.txt");
//$fileTime = strtotime($fileTime);

$curTime = date('r');
$curTime = strtotime($curTime);

// Get delta time in minutes
$deltaTime = intval(($curTime - $fileTime) / 60);

$cnt = 0;
while(!feof($myfile))
{
	$previousLine = $fileLine;
	$fileLine = fgets($myfile, 1024);

    // Separate string part to get the numbers
    $posFirstDelimiter = strpos($fileLine, ' ', 0);
    $posSecondDelimiter = strpos($fileLine, ' ', ($posFirstDelimiter + 1));
    $posThirdDelimiter = strpos($fileLine, ' ', ($posSecondDelimiter));

    $subStrChannel = substr($fileLine, 0, $posFirstDelimiter);
    $subStrGpio = substr($fileLine, ($posFirstDelimiter + 1), $posSecondDelimiter);
    $subStrStatus = substr($fileLine, ($posThirdDelimiter + 1));

    //$switchStatusArray[$cnt]['channel']=$subStrChannel;
    $switchStatusArray[$cnt]['gpio']=trim(preg_replace('/\s+/', ' ', $subStrGpio));
    $switchStatusArray[$cnt]['status']=trim(preg_replace('/\s+/', ' ', $subStrStatus));

    $cnt++;
}

// Output line
$fileLine = mb_convert_encoding($fileLine,"UTF-8","ISO-8859-1");

fclose($myfile);

$data = array('channelNumber'=>$channelNumber,
			  'gpioNumber'=>$gpioNumber,
			  'outputText'=>$switchStatusArray,
              'fileTime'=>$fileTime,
              'curTime'=>$curTime,
              'deltaTime'=>$deltaTime,
              'fileNumOfLines'=>$cnt,
             );
print json_encode($data);
?>
