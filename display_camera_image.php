<?php
// Header
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

// Get params
$value = $_GET['get_param'];

// User selection decision: if '1' is passed as first parameters we do automatically,
// otherwise all parameters are retrieved
if ($value == "1")
{
    // Send the command
    $cmdString = "raspistill -t 100 -n -o /webroot/logs/camera_image.jpg -w 640 -h 480 -q 100";
    $fileString = "./logs/camera_image.jpg";

    exec($cmdString); // Exec will execute external program, shell_exec execute via shell and return output as a string

    // sleep for 1 seconds
    sleep(1);
}
else
{
    // Get user inputs
    $cameraStatus = $_GET['cameraStatus_param'];
    $cameraAutoStatus = $_GET['cameraAutoStatus_param'];
    $cameraIso = $_GET['cameraIso_param'];
    $cameraFlashStatus = $_GET['cameraFlashStatus_param'];
    $cameraPictureSize = $_GET['cameraPictureSize_param'];
    $cameraExposure = $_GET['cameraExposure_param'];
    $cameraWhiteBalance = $_GET['cameraWhiteBalance_param'];
    $cameraSharpness = $_GET['cameraSharpness_param'];
    $cameraSaturation = $_GET['cameraSaturation_param'];
    $cameraBrightness = $_GET['cameraBrightness_param'];
    $cameraContrast = $_GET['cameraContrast_param'];
    $cameraDrc = $_GET['cameraDrc_param'];
    $cameraShutterSpeed = $_GET['cameraShutterSpeed_param'];

    // Get the last used number for naming file
    $imgNum = file_get_contents('/webroot/logs/last_picture_number.txt');

    if ($imgNum === false)
    {
        // Error
        $imgNum = $value;
    }
    else
    {
        // File exists we use the image number got from file incremented by 1
        $imgNum += 1;
    }

    // Write the new last used number, if the file does not exist it is created
    file_put_contents('/webroot/logs/last_picture_number.txt', $imgNum);

    // Set sharpness
    if ($cameraAutoStatus === "MANUAL")
    {
        $sharpnessStr = " -sh " . $cameraSharpness;
        $saturationStr = " -sa " . $cameraSaturation;
        $brightnessStr = " -br " . $cameraBrightness;
        $contrastStr = " -co " . $cameraContrast;

        $containerStr = $sharpnessStr . $saturationStr . $brightnessStr . $contrastStr;
    }
    else
    {
        $containerStr = "";
    }

    // Set DRC
    if ($cameraDrc === "OFF")
    {
        $drcStr = " -drc off";
    }
    else if ($cameraDrc === "LOW")
    {
        $drcStr = " -drc low";
    }
    else if ($cameraDrc === "MEDIUM")
    {
        $drcStr = " -drc medium";
    }
    else if ($cameraDrc === "HIGH")
    {
        $drcStr = " -drc high";
    }

    // White balance
    if ($cameraWhiteBalance === "AUTO")
    {
        $wbStr = " -awb auto";
    }
    else if ($cameraWhiteBalance === "OFF")
    {
        $wbStr = " -awb off";
    }
    else if ($cameraWhiteBalance === "SUNNY")
    {
        $wbStr = " -awb sun";
    }
    else if ($cameraWhiteBalance === "CLOUDY")
    {
        $wbStr = " -awb cloudshade";
    }
    else if ($cameraWhiteBalance === "FLASH")
    {
        $wbStr = " -awb flash";
    }
    else
    {
        $wbStr = "";
    }

    // Prepare exposure string only if we are in AUTO, in MANUAL ISO and Shutter Speed are used
    if ($cameraAutoStatus === "AUTO")
    {
        if (strcmp($cameraExposure, "NIGHT") === 0)
        {
            $exposureStr = " -ex night";
        }
        else if (strcmp($cameraExposure, "VERYLONG") === 0)
        {
            $exposureStr = " -ex verylong";
        }
        else if (strcmp($cameraExposure, "SPOTLIGHT") === 0)
        {
            $exposureStr = " -ex spotlight";
        }
        else // AUTO
        {
            $exposureStr = " -ex auto";
        }
    }
    else // In MANUAL mode Exposure settings are not used
    {
        $exposureStr = "";
    }

    // Prepare ISO string
    if ($cameraAutoStatus === "MANUAL")
    {
        if (strcmp($cameraIso, "200") === 0)
        {
            $isoStr = " -ISO 200";
        }
        else if (strcmp($cameraIso, "400") === 0)
        {
            $isoStr = " -ISO 400";
        }
        else if (strcmp($cameraIso, "800") === 0)
        {
            $isoStr = " -ISO 800";
        }
        else // 100
        {
            $isoStr = " -ISO 100";
        }
    }
    else // In AUTO mode ISO settings are not used
    {
        $isoStr = "";
    }

    // Prepare Shutter Speed string
    if ($cameraAutoStatus === "MANUAL")
    {
        $ssStr = " -ss " . $cameraShutterSpeed;
        //$ssStr = " -ss 25000"; // This is the maximum before camera lock-up (not tested by me)
    }
    else // In AUTO mode Shutter Speed settings are not used
    {
        $ssStr = "";
    }

    // Send the command deciding for picture dimansion
    if (strcmp($cameraPictureSize, "2592x1944") === 0)
    {
        $cmdString = "raspistill -t 100 -n -o /webroot/logs/camera_image_" . $imgNum . ".jpg -w 2592 -h 1944 -q 100" . $exposureStr . $isoStr . $ssStr . $wbStr . $containerStr . $drcStr;
    }
    else if (strcmp($cameraPictureSize, "1920x1080") === 0)
    {
        $cmdString = "raspistill -t 100 -n -o /webroot/logs/camera_image_" . $imgNum . ".jpg -w 1920 -h 1080 -q 100" . $exposureStr . $isoStr . $ssStr . $wbStr . $containerStr . $drcStr;
    }
    else if (strcmp($cameraPictureSize, "1296x972") === 0)
    {
        $cmdString = "raspistill -t 100 -n -o /webroot/logs/camera_image_" . $imgNum . ".jpg -w 1296 -h 972 -q 100" . $exposureStr . $isoStr . $ssStr . $wbStr . $containerStr . $drcStr;
    }
    else if (strcmp($cameraPictureSize, "1296x730") === 0)
    {
        $cmdString = "raspistill -t 100 -n -o /webroot/logs/camera_image_" . $imgNum . ".jpg -w 1296 -h 730 -q 100" . $exposureStr . $isoStr . $ssStr . $wbStr . $containerStr . $drcStr;
    }
    else // 'AUTO' or '640x480'
    {
        $cmdString = "raspistill -t 100 -n -o /webroot/logs/camera_image_" . $imgNum . ".jpg -w 640 -h 480 -q 100" . $exposureStr . $isoStr . $ssStr . $wbStr . $containerStr . $drcStr;
    }

    $fileString = "./logs/camera_image_" . $imgNum . ".jpg";

    exec($cmdString); // Exec will execute external program, shell_exec execute via shell and return output as a string
}

$image = file_get_contents($fileString);

if($image === FALSE)
{
    // We do not have a valid image
    $base64 = "";
}
else
{
    $base64 = base64_encode($image);
}

$title = "Log displayed: " . $imgNum;
$additionalData = $cmdString;

$data = array('base64picture'=>$base64,
              'title'=>$title,
              'additionalData'=>$cmdString,
             );

print json_encode($data);
?>
