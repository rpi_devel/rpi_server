<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$fileString = "./logs/console.txt";
$consoleText = file_get_contents($fileString);

if($consoleText === FALSE)
{
	// We do not have a valid file just return
	exit ();
}

$data = array('title'=>"Console text",
              'text'=>$consoleText,
             );

print json_encode($data);
?>
