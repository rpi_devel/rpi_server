<!DOCTYPE html PUBLIC "-/
/W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Temperature and Humidity </title>
		<script src="script/jquery-2.1.4.min.js"></script>
		<script src="script/jquery.qtip.min.js"></script>
		<script src="script/temperature_humidity.js"></script>
		<link rel="stylesheet" type="text/css" href="style/jquery.qtip.min.css" />
		<link rel="stylesheet" type="text/css" href="style/style.css" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>

	<!-- <body onload='drawTemp(); draw(0);'> -->
	<!-- <body onload='drawWithInputValue();'> -->
    <body class="page-id-001">
		<?php
		// Set timezone to avoid date() warning message
		date_default_timezone_set('Europe/Rome');
		?>
		<div class="wrapper" id="wrapper_id">
			<nav>
				<ul>
					<li>
						<a href="temperature_humidity.php">Temperature Page</a>
					</li>
					<li>
						<a href="accgyro.php">Acc/Gyro Page</a>
					</li>
					<li>
						<a href="control.php">Control</a>
					</li>
					<li>
						<a href="camera.php">Camera</a>
					</li>
					<li>
				        <a href="switch.php">Switches</a>
				    </li>					
					<li>
						<a href="index.php">Home</a>
					</li>
				</ul>
			</nav>

			<div id="LargeWidthTip"> <h2>Temperature Page</h2></div>
			<div id="load_temperature_page" class="text_user_message">Temperature sensor report...</div>
		</div>
		<div class="push"></div>

            <!-- Thermometer / Hydrometer canvas -->
            <div id="temperature_canvas_div_id"> <!-- style="position: absolute; top: 150px; left: 100px; z-index:1"> -->
				<canvas id="temperature" width="440" height="220">Canvas not available</canvas>
				</div>
			<div id="humidity_canvas_div_id"> <!-- style="position: absolute; top: 150px; left: 100px; z-index:1"> -->
				<canvas id="hydrometer" width="440" height="220">Canvas not available</canvas>
            </div>
			<div id="temperature_text_div_id">Temperature</div>
			<div id="humidity_text_div_id">Humidity</div>
            <!-- Hydrometer canvas -->
            <!-- <div id="humidity_canvas_div_id" style="float"> --><!-- style="position: absolute; top: 150px; left: 550px; z-index:1"> -->
		<div id="footer" class="footer_class">
			<p>Remote Control Site, &copy; 2015-<? echo date("Y")?> Our srl</p>
		</div>
	</body>
</html>
